<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = [
        'section_id',
        'name',
        'slug',
        'description',
        'status',
        'fees'
    ];

    protected $appends = ['section_name'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    public function section() {
        return $this->belongsTo(Section::class);
    }

    public function classrooms() {
        return $this->hasMany(Classroom::class);
    }

    public function courses() {
        return $this->belongsToMany(Course::class, 'group_course');
    }

    public function notes()
    {
        return $this->hasManyThrough(Note::class, Classroom::class);
    }

    public function getSectionNameAttribute() {
        return $this->section->name;
    }

}
